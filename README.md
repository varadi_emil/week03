# Homework Week 3

## Váradi Zoltán-Emil
##### Computational physics, year 2

### 0. Dependencies
openjdk 11
draw.io free and open source https://github.com/jgraph/drawio

### 1. Assignment 1 reupload
Logger
Build with "make"
Clean with "make clean"
Run with "make run"
Sources: Task1/

### 2. Shapes in java
Diagram for Task1
Made with "draw.io"
"hw_shape.zip" not found in assignment

### 3. 
"hw_shape.zip" not found in assignment

Sources: Task2/*