import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	public static int DEBUG = 0;
	public static int INFO = 1;
	public static int WARNING = 2;
	public static int ERROR = 3;

	public static int CONSOLE = 0;
	public static int FILE = 1;

	private String _id;
	private int _level = ERROR;
	private int _target = CONSOLE;
	private boolean _enabled = true;

	private FileWriter fileWriter;

	public void dispose() {
		if(this.fileWriter == null) {
			return;
		}

		try {
			this.fileWriter.close();
		}
		catch (Exception ex) {
			System.out.println("Failed to close fileWriter!");
		}
	}

	public void setId(String id) {
		this._id = id;
	}
	
	public String getId() {
		return this._id;
	}
	
	public boolean getEnabled() {
		return this._enabled;
	}

	public void setEnabled(boolean enabled) {
		this._enabled = enabled;
	}

	public void setLevel(int level) {
		this._level = level;
	}

	public int getLevel() {
		return this._level;
	}

	public void setTarget(int target) {
		/// making sure the _target is set to a valid value
		this._target = target % (FILE + 1);
	}

	public int getTarget() {
		return this._target;
	}
	
	public void error(String message) {
		if ( this._enabled && this._level <= ERROR) {
			sendToOutput(message, "error");
		}
	}

	public void warning(String message) {
		if ( this._enabled && this._level <= WARNING ) {
			sendToOutput(message, "warning");
		}
	}

	public void info(String message) {
		if ( this._enabled && this._level <= INFO) {
			sendToOutput(message, "info");
		}
	}

	public void debug(String message) {
		if ( this._enabled && this._level <= DEBUG) {
			sendToOutput(message, "debug");
		}
	}
        
	private String __getCallLocation() {
		StackTraceElement traceElement = new Throwable().getStackTrace()[2]; // 0th level-> Logger.__getCallLocation, 1st level -> e.g., Logger.warning(), 2nd level is of interest, e.g., LoggerTester.main
		return traceElement.getClassName()  + "." + 
				traceElement.getMethodName() + "(line " +  
				traceElement.getLineNumber() + ")";
	}        
        
	private void sendToOutput(String message, String messageType) {
		/// Splitting when on new lines, and avoiding empty lines.
		String lines[] = message.split("[\\r\\n]+");
		String type = String.format("[%s]", messageType.toUpperCase());

		output(type + " " + __getCallLocation() + ": " + lines[0]);
		for(int i = 1; i < lines.length; i++ ) {
			output(type + " " + lines[i]);
		}
	}

	private void output(String message) {
		if(this._target == CONSOLE) {
			print(message);
			return;
		}

		write(message);
	} 

	private void print(String message) {
		System.out.println(message);
	}

	private void write(String message) {
		if(this.fileWriter == null) {
			try {
			    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd>>HH-mm-ss");
				Date now = new Date();

			    String dateTime = dateFormat.format(now);
				String fileName = String.format("logs%s_%s.log", this._id, dateTime);
				this.fileWriter = new FileWriter(fileName);
			}
			catch (Exception ex) {
				System.out.println("Failed to create fileWriter!");
			}
		}

		writeToFile(this.fileWriter, message);
	}

	private void writeToFile(FileWriter writer, String string) {
        try {
           writer.write(string);
		   writer.write("\n");
        }
        catch (IOException e) {
            System.out.println("Logging gone wrong");
        }
    }
}