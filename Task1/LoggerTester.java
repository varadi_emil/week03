public class LoggerTester {
	public static int summation(int start, int end) {
		LoggerFacade.info("summation - starting...");
		if ( end < start ) {
			LoggerFacade.error("summation - 'end' less than 'start'");
			System.exit(1);
		}
		if ( end == start ) {
			LoggerFacade.warning("summation - 'end' equals 'start'");
		}
		int s = 0;
		for(int i = start; i <= end; i++) {
			s += i;
			LoggerFacade.debug("summation - i = " + i + " s = " + s);
		}
		LoggerFacade.info("summation - finished!");
		return s;
	}

	public static void multilineTest() {
		String message = "Lorem Ipsum is simply dummy\n" +
							"text of the printing and\n" +
							"typesetting industry. Lorem\n" +
							"Ipsum has been the industry's\n" +
							"standard dummy text ever since\n" +
						    "the 1500s, when an unknown\n" +
							"printer took a galley of type\n" +
							"and scrambled it to make a type\n" +
							"specimen book. It has survived\n" +
							"not only five centuries, but\n" +
						    "also the leap into electronic\n" +
							"typesetting, remaining essentially\n" +
							"unchanged. It was popularised in the\n" + 
							"1960s with the release of Letraset\n" + 
							"sheets containing Lorem Ipsum passages,\n" + 
							"and more recently with desktop\n" + 
							"publishing software like Aldus\n" + 
							"PageMaker including versions\n" +
							"of Lorem Ipsum.";
		LoggerFacade.debug(message);
		LoggerFacade.info(message);
		LoggerFacade.warning(message);
		LoggerFacade.error(message);
	}

	public static void main(String[] args) {
		LoggerFacade.createLogger(true, 0, 0);
		LoggerFacade.createLogger(true, 0, 1);
		LoggerFacade.createLogger(true, 1, 1);
		LoggerFacade.createLogger(true, 2, 1);
		LoggerFacade.createLogger(true, 3, 1);
		LoggerFacade.debug("trying DEBUG");
		LoggerFacade.info("trying INFO");
		LoggerFacade.warning("trying WARNING");
		LoggerFacade.error("trying ERROR");
		System.out.println(summation(1, 10));

		multilineTest();

		LoggerFacade.dispose();
	}

}
