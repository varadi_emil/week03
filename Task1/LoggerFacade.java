import java.util.ArrayList;

public class LoggerFacade {
    private static ArrayList<Logger> loggers = new ArrayList<Logger>();

    public static void createLogger(boolean enabled, int level, int target) {
        Logger l = new Logger();
        l.setEnabled(enabled);
        l.setLevel(level);
        l.setTarget(target);
        l.setId(((Integer) loggers.size()).toString());
            
        loggers.add(l);
    }
    
	public static void error(String message) {
		loggers.forEach((l) -> l.error(message));
	}

	public static void warning(String message) {
		loggers.forEach((l) -> l.warning(message));
	}

	public static void info(String message) {
		loggers.forEach((l) -> l.info(message));
	}

	public static void debug(String message) {
		loggers.forEach((l) -> l.debug(message));
	}

    public static void dispose() {
        loggers.forEach((l) -> l.dispose());
    }
}